# guest-notifier

Will get QMP notifications and show it in guest

## Compilation

```
CGO_ENABLED=0 GOOS=<windows|linux|darwin> go build -o <BINARY NAME> .
```

## Usage

```
./<BINARY NAME> "<NOTIFICATION TITLE>" "<NOTIFICATION TEXT>"
```

## Windows set up

This needs to be like this for the guest notifier to work:

![](img/win_setup.png)

