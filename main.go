package main

import (
    "os"

    "github.com/gen2brain/beeep"
)

func main() {
    err := beeep.Alert(os.Args[0], os.Args[1], "")
    if err != nil {
        panic(err)
    }
}

