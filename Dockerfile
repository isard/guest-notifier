FROM golang:1.17-alpine
WORKDIR /build

COPY go.mod /build
COPY go.sum /build
RUN go mod download

COPY main.go /build
